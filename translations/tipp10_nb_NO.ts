<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="68"/>
        <source>Press space bar to start</source>
        <translation>Trykk mellomromstast for å starte</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="93"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="95"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="98"/>
        <source>E&amp;xit Game</source>
        <translation>&amp;Avslutt spill</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="338"/>
        <source>Number of points:</source>
        <translation>Antall poeng:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="403"/>
        <source>Points:</source>
        <translation>Poeng:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="402"/>
        <source>Level</source>
        <translation>Nivå</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="414"/>
        <source>Press space bar to proceed</source>
        <translation>Trykk mellomromstast for å fortsette</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="45"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="73"/>
        <source>Characters</source>
        <translation>Tegn</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="74"/>
        <source>Target Errors</source>
        <translation>Feilsiktemål</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="75"/>
        <source>Actual Errors</source>
        <translation type="unfinished">Faktiske feil</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="76"/>
        <source>Frequency</source>
        <translation>Frekvens</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="77"/>
        <source>Error Rate</source>
        <translation>Feilrate</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="80"/>
        <source>This column shows all of the
characters typed</source>
        <translation>Denne kolonnen viser alle
skrevne tegn</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="84"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>Tegnet skulle ha blitt inntastet, men ble ikke det</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="87"/>
        <source>Character was mistyped</source>
        <translation>Feil tegn ble tastet</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="89"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>Denne kolonnen indikerer total forekomst
for hvert tegn</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="117"/>
        <source>Reset characters</source>
        <translation>Tilbakestill tegn</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="191"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="68"/>
        <source>The process will be aborted.</source>
        <translation>Prosessen vil bli avbrutt.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="71"/>
        <source>The update will be aborted.</source>
        <translation>Oppdateringen vil bli avbrutt.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="74"/>
        <source>The program will be aborted.</source>
        <translation>Programmet vil lukkes.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>Cannot load the program logo.</source>
        <translation>Kan ikke laste inn programlogoen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation type="unfinished">Kan ikke laste inn tastatur-punktbildefil</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="91"/>
        <source>Cannot load the timer background.</source>
        <translation>Kan ikke laste inn tidsurbakgrunn.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="94"/>
        <source>Cannot load the status bar background.</source>
        <translation>Kan ikke laste inn statusfeltsbakgrunn.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="121"/>
        <location filename="../widget/errormessage.cpp" line="125"/>
        <source>Connection to the database failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="128"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="132"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="136"/>
        <source>No lessons exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="139"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="104"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="109"/>
        <source>Cannot create the user database in your AppData directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="115"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your AppData directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="146"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="160"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="174"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="187"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="190"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="196"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="201"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="206"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="211"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="215"/>
        <source>Cannot create temporary file.</source>
        <translation>Kan ikke opprette midlertidig fil.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="218"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="222"/>
        <source>Cannot read the online update version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="225"/>
        <source>Cannot read the database update version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="228"/>
        <source>Cannot execute the SQL statement.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="234"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="237"/>
        <source>Cannot create analysis table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="240"/>
        <source>Cannot create analysis index.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="243"/>
        <source>Cannot fill analysis table with values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="247"/>
        <source>An error has occured.</source>
        <translation>En feil har oppstått.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>
(Error number: %1)
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="43"/>
        <source>Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="47"/>
        <source>Overview of Lessons</source>
        <translation>Oversikt over øvinger</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="48"/>
        <source>Progress of Lessons</source>
        <translation>Øvingsframdrift</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="49"/>
        <source>Characters</source>
        <translation>Tegn</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="50"/>
        <source>Fingers</source>
        <translation>Fingre</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="51"/>
        <source>Comparison Table</source>
        <translation>Sammenligningstabell</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="72"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Lukk</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="143"/>
        <source>Use examples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="147"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="157"/>
        <source>Score</source>
        <translation type="unfinished">Poengsum</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="162"/>
        <source>For example, this equates to …</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="167"/>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="174"/>
        <location filename="../widget/evaluationwidget.cpp" line="190"/>
        <location filename="../widget/evaluationwidget.cpp" line="201"/>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <location filename="../widget/evaluationwidget.cpp" line="228"/>
        <location filename="../widget/evaluationwidget.cpp" line="239"/>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <location filename="../widget/evaluationwidget.cpp" line="266"/>
        <location filename="../widget/evaluationwidget.cpp" line="277"/>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <location filename="../widget/evaluationwidget.cpp" line="304"/>
        <location filename="../widget/evaluationwidget.cpp" line="315"/>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <location filename="../widget/evaluationwidget.cpp" line="342"/>
        <location filename="../widget/evaluationwidget.cpp" line="353"/>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <location filename="../widget/evaluationwidget.cpp" line="380"/>
        <location filename="../widget/evaluationwidget.cpp" line="391"/>
        <source>Points</source>
        <translation>Poeng</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="178"/>
        <location filename="../widget/evaluationwidget.cpp" line="194"/>
        <location filename="../widget/evaluationwidget.cpp" line="205"/>
        <location filename="../widget/evaluationwidget.cpp" line="216"/>
        <location filename="../widget/evaluationwidget.cpp" line="232"/>
        <location filename="../widget/evaluationwidget.cpp" line="243"/>
        <location filename="../widget/evaluationwidget.cpp" line="254"/>
        <location filename="../widget/evaluationwidget.cpp" line="270"/>
        <location filename="../widget/evaluationwidget.cpp" line="281"/>
        <location filename="../widget/evaluationwidget.cpp" line="292"/>
        <location filename="../widget/evaluationwidget.cpp" line="308"/>
        <location filename="../widget/evaluationwidget.cpp" line="319"/>
        <location filename="../widget/evaluationwidget.cpp" line="330"/>
        <location filename="../widget/evaluationwidget.cpp" line="346"/>
        <location filename="../widget/evaluationwidget.cpp" line="357"/>
        <location filename="../widget/evaluationwidget.cpp" line="368"/>
        <location filename="../widget/evaluationwidget.cpp" line="384"/>
        <location filename="../widget/evaluationwidget.cpp" line="395"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="183"/>
        <source>No experience in touch typing</source>
        <translation>Ingen erfaring med motorikkskriving</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="221"/>
        <source>First steps in touch typing</source>
        <translation type="unfinished">Nybegynner på motorikkskriving</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="259"/>
        <source>Advanced level</source>
        <translation>Avansert nivå</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="297"/>
        <source>Suitable skills</source>
        <translation>Brukbare ferdigheter</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="335"/>
        <source>Very good skills</source>
        <translation>Veldig gode ferdigheter</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="373"/>
        <source>Perfect skills</source>
        <translation>Perfekte ferdigheter</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="233"/>
        <source>Error rates of your fingers</source>
        <translation>Feilrate for fingrene dine</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="240"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>Error Rate:</source>
        <translation>Feilrate:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="253"/>
        <source>Frequency:</source>
        <translation>Frekvens:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="255"/>
        <source>Errors:</source>
        <translation>Feil:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="30"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="41"/>
        <location filename="../widget/helpbrowser.cpp" line="44"/>
        <source>en</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="72"/>
        <source>Back</source>
        <translation>Tilbake</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="75"/>
        <source>Table of Contents</source>
        <translation>Innholdsfortegnelse</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="76"/>
        <source>&amp;Close</source>
        <translation>&amp;Lukk</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="79"/>
        <source>&amp;Print page</source>
        <translation>&amp;Skriv ut siden</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="118"/>
        <source>Print page</source>
        <translation>Skriv ut</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="29"/>
        <source>Introduction</source>
        <translation>Introduksjon</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="60"/>
        <source>Welcome to TIPP10</source>
        <translation>Velkommen til TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="65"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="72"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Tips for bruk av tifingersystemet</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="82"/>
        <source>en</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="86"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="91"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="97"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="100"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="104"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="108"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="119"/>
        <source>All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="133"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Kjør TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="138"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>&amp;Ikke vis dette vinduet igjen</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="102"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="103"/>
        <source>&amp;Save</source>
        <translation>&amp;Lagre</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="112"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="114"/>
        <source>Short description (120 characters max.):</source>
        <translation>Kort beskrivelse (maks. 120 tegn.):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="115"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Øvingsinnhold (minst to linjer):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="146"/>
        <source>Sentence Lesson</source>
        <translation>Setningsøving</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="148"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Hver linje vil fullføres
med linjeskift til slutt</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="153"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="160"/>
        <source>Edit own Lesson</source>
        <translation>Rediger egen øving</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="226"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Skriv inn navn på øvingen
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="232"/>
        <source>Please enter entire lesson content
</source>
        <translation>Skriv inn hele øvingsinnholdet
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="255"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Skriv inn maks. 400 linjer tekst
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="263"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="151"/>
        <source>Word Lesson</source>
        <translation>Ord-øving</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="117"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="127"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="135"/>
        <source>Dictate the text as:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="162"/>
        <source>Name of the Lesson:</source>
        <translation>Navn på øvingen:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="166"/>
        <source>Create own Lesson</source>
        <translation>Opprett din egen øving</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="249"/>
        <source>Please enter at least two lines of text
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="25"/>
        <source>Print Lesson</source>
        <translation>Skriv ut øving</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="45"/>
        <source>&amp;Print</source>
        <translation>&amp;Skriv ut</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="46"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="60"/>
        <source>Please enter your name:</source>
        <translation>Skriv inn navnet ditt:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="51"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="83"/>
        <source>Entire Lesson</source>
        <translation>Hele øvingen</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="178"/>
        <source>%L1 min</source>
        <translation type="unfinished">%L1 min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="254"/>
        <location filename="../widget/lessonresult.cpp" line="449"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="272"/>
        <location filename="../widget/lessonresult.cpp" line="467"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="283"/>
        <location filename="../widget/lessonresult.cpp" line="330"/>
        <location filename="../widget/lessonresult.cpp" line="479"/>
        <location filename="../widget/lessonresult.cpp" line="525"/>
        <source>Duration: </source>
        <translation>Varighet: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="287"/>
        <location filename="../widget/lessonresult.cpp" line="483"/>
        <source>Typing Errors: </source>
        <translation>Skrivefeil: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="291"/>
        <location filename="../widget/lessonresult.cpp" line="487"/>
        <source>Assistance: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="305"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Results</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="322"/>
        <location filename="../widget/lessonresult.cpp" line="517"/>
        <source>Lesson: </source>
        <translation>Øving: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="326"/>
        <location filename="../widget/lessonresult.cpp" line="521"/>
        <source>Time: </source>
        <translation>Tid: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="334"/>
        <location filename="../widget/lessonresult.cpp" line="529"/>
        <source>Characters: </source>
        <translation>Tegn: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="338"/>
        <location filename="../widget/lessonresult.cpp" line="533"/>
        <source>Errors: </source>
        <translation>Feil: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="342"/>
        <location filename="../widget/lessonresult.cpp" line="537"/>
        <source>Error Rate: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="346"/>
        <location filename="../widget/lessonresult.cpp" line="541"/>
        <source>Cpm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="360"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Dictation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="443"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source>Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source> of %1</source>
        <translation> av %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="571"/>
        <source>Print Report</source>
        <translation>Skriv ut rapport</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="73"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minutt</numerusform>
            <numerusform>%n minutter</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="78"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n tegn</numerusform>
            <numerusform>%n tegn</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="90"/>
        <source>Error Correction with Backspace</source>
        <translation>Feilkorrigering med rettetast</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="93"/>
        <source>Error Correction without Backspace</source>
        <translation>Feilkorrigering uten rettetast</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="95"/>
        <source>Ignore Errors</source>
        <translation>Ignorer feil</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="111"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="119"/>
        <source>- Colored Keys</source>
        <translation>- Fargede taster</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="125"/>
        <source>- Home Row</source>
        <translation>- Hjemmerad</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="131"/>
        <source>- Motion Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="137"/>
        <source>- Separation Line</source>
        <translation type="unfinished">- Inndelingslinje</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="143"/>
        <source>- Instructions</source>
        <translation>- Instruks</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="175"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n poeng</numerusform>
            <numerusform>%n poeng</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="69"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="73"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="77"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="89"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n poeng</numerusform>
            <numerusform>%n poeng</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="126"/>
        <source>Show: </source>
        <translation>Vis: </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="128"/>
        <source>All Lessons</source>
        <translation>Alle øvinger</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="129"/>
        <source>Training Lessons</source>
        <translation>Treningsøvinger</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="130"/>
        <source>Open Lessons</source>
        <translation>Åpne øvinger</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="131"/>
        <source>Own Lessons</source>
        <translation>Egne øvinger</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="266"/>
        <source>Lesson</source>
        <translation>Øving</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="267"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="268"/>
        <source>Duration</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="269"/>
        <source>Characters</source>
        <translation>Tegn</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="270"/>
        <source>Errors</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="271"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="272"/>
        <source>Cpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="273"/>
        <source>Score</source>
        <translation>Poengsum</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="276"/>
        <source>This column shows the names
of completed lessons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="280"/>
        <source>Start time of the lesson</source>
        <translation>Øvingens starttid</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="282"/>
        <source>Total duration of the lesson</source>
        <translation>Øvingens totale varighet</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="283"/>
        <source>Number of characters dictated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="286"/>
        <source>Number of typing errors</source>
        <translation>Antall skrivefeil</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="61"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="82"/>
        <location filename="../widget/mainwindow.cpp" line="100"/>
        <source>&amp;Go</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="88"/>
        <location filename="../widget/mainwindow.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="96"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Settings</source>
        <translation>&amp;Innstillinger</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="116"/>
        <source>E&amp;xit</source>
        <translation>&amp;Avslutt</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="118"/>
        <source>&amp;Results</source>
        <translation>&amp;Resultater</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manuell</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="121"/>
        <source> on the web</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="123"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="125"/>
        <source>&amp;About </source>
        <translation>&amp;Om </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="128"/>
        <source>ABC-Game</source>
        <translation>ABC-spill</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="182"/>
        <source>Software Version </source>
        <translation>Programvareversjon </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="183"/>
        <source>Database Version </source>
        <translation>Databaseversjon </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="188"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="189"/>
        <source>On the web: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="191"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="201"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="185"/>
        <source>Portable Version</source>
        <translation>Portabel versjon</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="39"/>
        <location filename="../widget/progressionwidget.cpp" line="50"/>
        <location filename="../widget/progressionwidget.cpp" line="504"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="42"/>
        <source>Show: </source>
        <translation>Vis: </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="44"/>
        <source>All Lessons</source>
        <translation>Alle øvinger</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="45"/>
        <source>Training Lessons</source>
        <translation>Treningsøvinger</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="46"/>
        <source>Open Lessons</source>
        <translation>Åpne øvinger</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="47"/>
        <source>Own Lessons</source>
        <translation>Egne øvinger</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <source>Order by x-axis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="51"/>
        <location filename="../widget/progressionwidget.cpp" line="510"/>
        <source>Lesson</source>
        <translation>Øving</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="229"/>
        <source>Points</source>
        <translation>Poeng</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="360"/>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <source>Training Lesson</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="367"/>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <source>Open Lesson</source>
        <translation>Åpne øving</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="374"/>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <source>Own Lesson</source>
        <translation>Egen øving</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="455"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n poeng</numerusform>
            <numerusform>%n poeng</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="456"/>
        <source>%1 cpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="473"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="515"/>
        <source>Cpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="520"/>
        <source>Score</source>
        <translation>Poengsum</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="97"/>
        <location filename="../sql/connection.h" line="105"/>
        <location filename="../sql/connection.h" line="125"/>
        <source>Affected directory:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>en</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="31"/>
        <location filename="../widget/fingerwidget.cpp" line="120"/>
        <source>Left little finger</source>
        <translation>Venstre lillefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="32"/>
        <location filename="../widget/fingerwidget.cpp" line="121"/>
        <source>Left ring finger</source>
        <translation>Venstre ringfinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="33"/>
        <location filename="../widget/fingerwidget.cpp" line="122"/>
        <source>Left middle finger</source>
        <translation>Venstre langefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="123"/>
        <source>Left forefinger</source>
        <translation>Venstre pekefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="124"/>
        <source>Right forefinger</source>
        <translation>Høyre pekefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="35"/>
        <location filename="../widget/fingerwidget.cpp" line="125"/>
        <source>Right middle finger</source>
        <translation>Høyre langefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="36"/>
        <location filename="../widget/fingerwidget.cpp" line="126"/>
        <source>Right ring finger</source>
        <translation>Høyre ringfinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Right little finger</source>
        <translation>Høyre lillefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Thumb</source>
        <translation>Tommel</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation>Erstatningsfilter</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="74"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="291"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="305"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="316"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="330"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Trening</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation>Her kan du velge bakgrunnsfargen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation>Her kan du velge skriftfargen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation>Peker:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>Endre &amp;skrift</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Skrift:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation>Skriftfarge:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation>Bakgrunn:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Hastighet:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Av</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Rask</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Lydutdata</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Treningsøvinger:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation type="unfinished">Resultat</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>Brukerdata</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation>Vinduer</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="87"/>
        <location filename="../widget/startwidget.cpp" line="731"/>
        <source>Training Lessons</source>
        <translation type="unfinished">Treningsøvinger</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="94"/>
        <source>Subject:</source>
        <translation>Emne:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="146"/>
        <source>&amp;Edit</source>
        <translation>&amp;Rediger</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="150"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;Ny øving</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="152"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Importer øving</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="154"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Eksporter øving</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="156"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Rediger øving</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="158"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Slett øving</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="187"/>
        <source>Duration of Lesson</source>
        <translation>Øvingens varighet</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="190"/>
        <source>Time Limit:</source>
        <translation>Tidsgrense:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="192"/>
        <location filename="../widget/startwidget.cpp" line="199"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="198"/>
        <source> minutes</source>
        <translation> minutter</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="203"/>
        <source>Character Limit:</source>
        <translation>Tegngrense:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="213"/>
        <source> characters</source>
        <translation> tegn</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <location filename="../widget/startwidget.cpp" line="215"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="219"/>
        <source>Entire
Lesson</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="222"/>
        <source>(Entire Lesson)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="252"/>
        <source>Response to Typing Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="255"/>
        <source>Block Typing Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="257"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="260"/>
        <source>Correction with Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="262"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="265"/>
        <source>Audible Signal</source>
        <translation>Lydsignal</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="266"/>
        <source>A beep sounds with every typing error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="269"/>
        <source>Show key picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="276"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="282"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="289"/>
        <source>Intelligence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="291"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="323"/>
        <source>Assistance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="325"/>
        <source>Show Keyboard</source>
        <translation>Vis tastatur</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="326"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="330"/>
        <source>Colored Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="332"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="335"/>
        <source>Home Row</source>
        <translation>Hjemmerad</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="337"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>L/R Separation Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="347"/>
        <source>Instruction</source>
        <translation>Instruks</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="349"/>
        <source>Show fingers to be used in the status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="352"/>
        <source>Motion Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="354"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="401"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="402"/>
        <source>&amp;Start Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="523"/>
        <source>All</source>
        <translation type="unfinished">Alle</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="743"/>
        <source>Open Lessons</source>
        <translation>Åpne øvinger</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="773"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="782"/>
        <source>Own Lessons</source>
        <translation>Egne øvinger</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="906"/>
        <source>Please select a text file</source>
        <translation>Velg en tekstfil</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="907"/>
        <source>Text files (*.txt)</source>
        <translation>Tekstfiler (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1029"/>
        <source>Please indicate the location of a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1078"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="120"/>
        <source>Press space bar to proceed</source>
        <translation>Trykk mellomromstast for å fortsette</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="137"/>
        <source>Dictation Finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="99"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Avbryt</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="112"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="214"/>
        <source>Press space bar to start</source>
        <translation>Trykk mellomromstast for å starte</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="215"/>
        <source>Take Home Row position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="231"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="245"/>
        <source>Do you want to save your results?</source>
        <translation>Vil du lagre resultatene dine?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="462"/>
        <source>E&amp;xit Lesson early</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="515"/>
        <source>Errors: </source>
        <translation>Feil: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="517"/>
        <source>Cpm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="518"/>
        <source>Time: </source>
        <translation>Tid: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="520"/>
        <source>Characters: </source>
        <translation>Tegn: </translation>
    </message>
</context>
</TS>
