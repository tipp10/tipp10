<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="68"/>
        <source>Press space bar to start</source>
        <translation>Press space bar to start</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="93"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="95"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="98"/>
        <source>E&amp;xit Game</source>
        <translation>E&amp;xit Game</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="338"/>
        <source>Number of points:</source>
        <translation>Number of points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="403"/>
        <source>Points:</source>
        <translation>Points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="402"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="414"/>
        <source>Press space bar to proceed</source>
        <translation>Press space bar to proceed</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="45"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="73"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="74"/>
        <source>Target Errors</source>
        <translation>Target Errors</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="75"/>
        <source>Actual Errors</source>
        <translation>Actual Errors</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="76"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="77"/>
        <source>Error Rate</source>
        <translation>Error Rate</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="80"/>
        <source>This column shows all of the
characters typed</source>
        <translation>This column shows all of the
characters typed</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="84"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>The character was supposed to be typed, but wasn&apos;t</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="87"/>
        <source>Character was mistyped</source>
        <translation>Character was mistyped</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="89"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>This column indicates the total frequency of each
character shown</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="117"/>
        <source>Reset characters</source>
        <translation>Reset characters</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="191"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="68"/>
        <source>The process will be aborted.</source>
        <translation>The process will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="71"/>
        <source>The update will be aborted.</source>
        <translation>The update will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="74"/>
        <source>The program will be aborted.</source>
        <translation>The program will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>Cannot load the program logo.</source>
        <translation>Cannot load the program logo.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Cannot load the keyboard bitmap.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="91"/>
        <source>Cannot load the timer background.</source>
        <translation>Cannot load the timer background.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="94"/>
        <source>Cannot load the status bar background.</source>
        <translation>Cannot load the status bar background.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</translation>
    </message>
    <message>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation type="vanished">Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="121"/>
        <location filename="../widget/errormessage.cpp" line="125"/>
        <source>Connection to the database failed.</source>
        <translation>Connection to the database failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="128"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>The user table with lesson data  cannot be emptied.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="132"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>The user table with error data cannot be emptied.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="136"/>
        <source>No lessons exist.</source>
        <translation>No lessons exist.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="139"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>No lesson selected.
Please select a lesson.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Cannot create the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="104"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="109"/>
        <source>Cannot create the user database in your AppData directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="115"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your AppData directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="146"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="160"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="174"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="187"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Cannot save the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="190"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Cannot retrieve the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Cannot analyze the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="196"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>The file could not be imported.
Please check whether it is a readable text file.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="201"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="206"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="211"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>The file could not be exported.
Please check to see whether it is a writable text file.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="215"/>
        <source>Cannot create temporary file.</source>
        <translation>Cannot create temporary file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="218"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Cannot execute the update process.
Please check your internet connection and proxy settings.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="222"/>
        <source>Cannot read the online update version.</source>
        <translation>Cannot read the online update version.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="225"/>
        <source>Cannot read the database update version.</source>
        <translation>Cannot read the database update version.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="228"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>Cannot execute the SQL statement.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Cannot find typing mistake definitions.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="234"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Cannot create temporary file.
Update failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="237"/>
        <source>Cannot create analysis table.</source>
        <translation>Cannot create analysis table.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="240"/>
        <source>Cannot create analysis index.</source>
        <translation>Cannot create analysis index.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="243"/>
        <source>Cannot fill analysis table with values.</source>
        <translation>Cannot fill analysis table with values.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="247"/>
        <source>An error has occured.</source>
        <translation>An error has occured.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Error number: %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="43"/>
        <source>Report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="47"/>
        <source>Overview of Lessons</source>
        <translation>Overview of Lessons</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="48"/>
        <source>Progress of Lessons</source>
        <translation>Progress of Lessons</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="49"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="50"/>
        <source>Fingers</source>
        <translation>Fingers</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="51"/>
        <source>Comparison Table</source>
        <translation>Comparison Table</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="72"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="143"/>
        <source>Use examples</source>
        <translation>Use examples</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="147"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="157"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="162"/>
        <source>For example, this equates to …</source>
        <translation>For example, this equates to …</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="167"/>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="174"/>
        <location filename="../widget/evaluationwidget.cpp" line="190"/>
        <location filename="../widget/evaluationwidget.cpp" line="201"/>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <location filename="../widget/evaluationwidget.cpp" line="228"/>
        <location filename="../widget/evaluationwidget.cpp" line="239"/>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <location filename="../widget/evaluationwidget.cpp" line="266"/>
        <location filename="../widget/evaluationwidget.cpp" line="277"/>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <location filename="../widget/evaluationwidget.cpp" line="304"/>
        <location filename="../widget/evaluationwidget.cpp" line="315"/>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <location filename="../widget/evaluationwidget.cpp" line="342"/>
        <location filename="../widget/evaluationwidget.cpp" line="353"/>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <location filename="../widget/evaluationwidget.cpp" line="380"/>
        <location filename="../widget/evaluationwidget.cpp" line="391"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="178"/>
        <location filename="../widget/evaluationwidget.cpp" line="194"/>
        <location filename="../widget/evaluationwidget.cpp" line="205"/>
        <location filename="../widget/evaluationwidget.cpp" line="216"/>
        <location filename="../widget/evaluationwidget.cpp" line="232"/>
        <location filename="../widget/evaluationwidget.cpp" line="243"/>
        <location filename="../widget/evaluationwidget.cpp" line="254"/>
        <location filename="../widget/evaluationwidget.cpp" line="270"/>
        <location filename="../widget/evaluationwidget.cpp" line="281"/>
        <location filename="../widget/evaluationwidget.cpp" line="292"/>
        <location filename="../widget/evaluationwidget.cpp" line="308"/>
        <location filename="../widget/evaluationwidget.cpp" line="319"/>
        <location filename="../widget/evaluationwidget.cpp" line="330"/>
        <location filename="../widget/evaluationwidget.cpp" line="346"/>
        <location filename="../widget/evaluationwidget.cpp" line="357"/>
        <location filename="../widget/evaluationwidget.cpp" line="368"/>
        <location filename="../widget/evaluationwidget.cpp" line="384"/>
        <location filename="../widget/evaluationwidget.cpp" line="395"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 cpm and %2 errors in %3 minutes</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="183"/>
        <source>No experience in touch typing</source>
        <translation>No experience in touch typing</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="221"/>
        <source>First steps in touch typing</source>
        <translation>First steps in touch typing</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="259"/>
        <source>Advanced level</source>
        <translation>Advanced level</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="297"/>
        <source>Suitable skills</source>
        <translation>Suitable skills</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="335"/>
        <source>Very good skills</source>
        <translation>Very good skills</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="373"/>
        <source>Perfect skills</source>
        <translation>Perfect skills</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="233"/>
        <source>Error rates of your fingers</source>
        <translation>Error rates of your fingers</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="240"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>The error rate is based on the recorded characters and the current selected keyboard layout.</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>Error Rate:</source>
        <translation>Error Rate:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="253"/>
        <source>Frequency:</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="255"/>
        <source>Errors:</source>
        <translation>Errors:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="30"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="41"/>
        <location filename="../widget/helpbrowser.cpp" line="44"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="72"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="75"/>
        <source>Table of Contents</source>
        <translation>Table of Contents</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="76"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="79"/>
        <source>&amp;Print page</source>
        <translation>&amp;Print page</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="118"/>
        <source>Print page</source>
        <translation>Print page</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="29"/>
        <source>Introduction</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="60"/>
        <source>Welcome to TIPP10</source>
        <translation>Welcome to TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="65"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="72"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Tips for using the 10 finger system</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="82"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="86"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="91"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="97"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Try to remain relaxed during the typing lessons.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="100"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="104"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="108"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>If you need assistance with using the software use the help function.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="119"/>
        <source>All rights reserved.</source>
        <translation>All rights reserved.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="133"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Launch TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="138"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>Do&amp;n&apos;t show me this window again</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="102"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="103"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="112"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Name of the lesson (20 characters max.):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="114"/>
        <source>Short description (120 characters max.):</source>
        <translation>Short description (120 characters max.):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="115"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Lesson content (at least two lines):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="146"/>
        <source>Sentence Lesson</source>
        <translation>Sentence Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="148"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Every line will be completed with
a line break at the end</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="153"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="160"/>
        <source>Edit own Lesson</source>
        <translation>Edit own Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="226"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Please enter the name of the lesson
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="232"/>
        <source>Please enter entire lesson content
</source>
        <translation>Please enter entire lesson content
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="255"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Please enter 400 lines of text maximum
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="263"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>The name of the lesson already exists. Please enter a new lesson name.
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="151"/>
        <source>Word Lesson</source>
        <translation>Word Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="117"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="127"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="135"/>
        <source>Dictate the text as:</source>
        <translation>Dictate the text as:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="162"/>
        <source>Name of the Lesson:</source>
        <translation>Name of the Lesson:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="166"/>
        <source>Create own Lesson</source>
        <translation>Create own Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="249"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Please enter at least two lines of text
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="25"/>
        <source>Print Lesson</source>
        <translation>Print Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="45"/>
        <source>&amp;Print</source>
        <translation>&amp;Print</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="46"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="60"/>
        <source>Please enter your name:</source>
        <translation>Please enter your name:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="51"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="83"/>
        <source>Entire Lesson</source>
        <translation>Entire Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="178"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="254"/>
        <location filename="../widget/lessonresult.cpp" line="449"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>You have reached %1 at a typing speed of %2 cpm and %n typing error.</numerusform>
            <numerusform>You have reached %1 at a typing speed of %2 cpm and %n typing errors.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="272"/>
        <location filename="../widget/lessonresult.cpp" line="467"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="283"/>
        <location filename="../widget/lessonresult.cpp" line="330"/>
        <location filename="../widget/lessonresult.cpp" line="479"/>
        <location filename="../widget/lessonresult.cpp" line="525"/>
        <source>Duration: </source>
        <translation>Duration: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="287"/>
        <location filename="../widget/lessonresult.cpp" line="483"/>
        <source>Typing Errors: </source>
        <translation>Typing Errors: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="291"/>
        <location filename="../widget/lessonresult.cpp" line="487"/>
        <source>Assistance: </source>
        <translation>Assistance: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="305"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="322"/>
        <location filename="../widget/lessonresult.cpp" line="517"/>
        <source>Lesson: </source>
        <translation>Lesson: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="326"/>
        <location filename="../widget/lessonresult.cpp" line="521"/>
        <source>Time: </source>
        <translation>Time: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="334"/>
        <location filename="../widget/lessonresult.cpp" line="529"/>
        <source>Characters: </source>
        <translation>Characters: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="338"/>
        <location filename="../widget/lessonresult.cpp" line="533"/>
        <source>Errors: </source>
        <translation>Errors: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="342"/>
        <location filename="../widget/lessonresult.cpp" line="537"/>
        <source>Error Rate: </source>
        <translation>Error Rate: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="346"/>
        <location filename="../widget/lessonresult.cpp" line="541"/>
        <source>Cpm: </source>
        <translation>Cpm: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="360"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Dictation</source>
        <translation>Dictation</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="443"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>TIPP10 Touch Typing Tutor</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source>Report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source> of %1</source>
        <translation> of %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="571"/>
        <source>Print Report</source>
        <translation>Print Report</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="73"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="78"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n character</numerusform>
            <numerusform>%n characters</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="90"/>
        <source>Error Correction with Backspace</source>
        <translation>Error Correction with Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="93"/>
        <source>Error Correction without Backspace</source>
        <translation>Error Correction without Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="95"/>
        <source>Ignore Errors</source>
        <translation>Ignore Errors</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="111"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="119"/>
        <source>- Colored Keys</source>
        <translation>- Colored Keys</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="125"/>
        <source>- Home Row</source>
        <translation>- Home Row</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="131"/>
        <source>- Motion Paths</source>
        <translation>- Motion Paths</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="137"/>
        <source>- Separation Line</source>
        <translation>- Separation Line</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="143"/>
        <source>- Instructions</source>
        <translation>- Instructions</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="175"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="69"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="73"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="77"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="89"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="126"/>
        <source>Show: </source>
        <translation>Show: </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="128"/>
        <source>All Lessons</source>
        <translation>All Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="129"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="130"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="131"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="266"/>
        <source>Lesson</source>
        <translation>Lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="267"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="268"/>
        <source>Duration</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="269"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="270"/>
        <source>Errors</source>
        <translation>Errors</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="271"/>
        <source>Rate</source>
        <translation>Rate</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="272"/>
        <source>Cpm</source>
        <translation>Cpm</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="273"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="276"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>This column shows the names
of completed lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="280"/>
        <source>Start time of the lesson</source>
        <translation>Start time of the lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="282"/>
        <source>Total duration of the lesson</source>
        <translation>Total duration of the lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="283"/>
        <source>Number of characters dictated</source>
        <translation>Number of characters dictated</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="286"/>
        <source>Number of typing errors</source>
        <translation>Number of typing errors</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="61"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>All results of the current lesson will be discarded!

Do you really want to exit?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="82"/>
        <location filename="../widget/mainwindow.cpp" line="100"/>
        <source>&amp;Go</source>
        <translation>&amp;Go</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="88"/>
        <location filename="../widget/mainwindow.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="96"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Settings</source>
        <translation>&amp;Settings</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="116"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="118"/>
        <source>&amp;Results</source>
        <translation>&amp;Results</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manual</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="121"/>
        <source> on the web</source>
        <translation> on the web</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="123"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="125"/>
        <source>&amp;About </source>
        <translation>&amp;About </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="128"/>
        <source>ABC-Game</source>
        <translation>ABC-Game</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="182"/>
        <source>Software Version </source>
        <translation>Software Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="183"/>
        <source>Database Version </source>
        <translation>Database Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="188"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Intelligent Touch Typing Tutor</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="189"/>
        <source>On the web: </source>
        <translation>On the web: </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="191"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="201"/>
        <source>About </source>
        <translation>About </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="185"/>
        <source>Portable Version</source>
        <translation>Portable Version</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="39"/>
        <location filename="../widget/progressionwidget.cpp" line="50"/>
        <location filename="../widget/progressionwidget.cpp" line="504"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="42"/>
        <source>Show: </source>
        <translation>Show: </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="44"/>
        <source>All Lessons</source>
        <translation>All Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="45"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="46"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="47"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <source>Order by x-axis:</source>
        <translation>Order by x-axis:</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="51"/>
        <location filename="../widget/progressionwidget.cpp" line="510"/>
        <source>Lesson</source>
        <translation>Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="229"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="360"/>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <source>Training Lesson</source>
        <translation>Training Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="367"/>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <source>Open Lesson</source>
        <translation>Open Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="374"/>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <source>Own Lesson</source>
        <translation>Own Lesson</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="455"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="456"/>
        <source>%1 cpm</source>
        <translation>%1 cpm</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="473"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>The progress graph will be shown after completing two lessons.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="515"/>
        <source>Cpm</source>
        <translation>Cpm</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="520"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="97"/>
        <location filename="../sql/connection.h" line="105"/>
        <location filename="../sql/connection.h" line="125"/>
        <source>Affected directory:
</source>
        <translation>Affected directory:
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="31"/>
        <location filename="../widget/fingerwidget.cpp" line="120"/>
        <source>Left little finger</source>
        <translation>Left little finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="32"/>
        <location filename="../widget/fingerwidget.cpp" line="121"/>
        <source>Left ring finger</source>
        <translation>Left ring finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="33"/>
        <location filename="../widget/fingerwidget.cpp" line="122"/>
        <source>Left middle finger</source>
        <translation>Left middle finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="123"/>
        <source>Left forefinger</source>
        <translation>Left forefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="124"/>
        <source>Right forefinger</source>
        <translation>Right forefinger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="35"/>
        <location filename="../widget/fingerwidget.cpp" line="125"/>
        <source>Right middle finger</source>
        <translation>Right middle finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="36"/>
        <location filename="../widget/fingerwidget.cpp" line="126"/>
        <source>Right ring finger</source>
        <translation>Right ring finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Right little finger</source>
        <translation>Right little finger</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Thumb</source>
        <translation>Thumb</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation>Filter for the keyboard layout</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation>Limitation of characters</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation>Replacement Filter</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="74"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Some of your settings require a restart of the software to take effect.
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="291"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="305"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>The data for completed lessons was successfully deleted!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="316"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="330"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>The recorded characters were successfully deleted!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Training</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation>Here you can select the background color</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation>Here you can select the font color</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation>Cursor:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>Change &amp;Font</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Here can select the color of the cursor for the current character</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Font:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation>Font Color:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation>Background:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Speed:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Fast</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Audio Output</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation>Here you can activate a metronome</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation>Metronome:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation>Select how often the metronome sound should appear per minute</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation> cpm</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Training Lessons:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation>Keyboard Layout:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>User Data</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation>Reset &amp;completed lessons</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>Reset all &amp;recorded characters</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation>Windows</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Here you can decide if an information window with tips is shown at the beginning</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation>Show welcome message at startup</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Remember enabled intelligence before starting
an open or own lesson</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="87"/>
        <location filename="../widget/startwidget.cpp" line="731"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="94"/>
        <source>Subject:</source>
        <translation>Subject:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="146"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="150"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;New Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="152"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Import Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="154"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Export Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="156"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Edit Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="158"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Delete Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="187"/>
        <source>Duration of Lesson</source>
        <translation>Duration of Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="190"/>
        <source>Time Limit:</source>
        <translation>Time Limit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="192"/>
        <location filename="../widget/startwidget.cpp" line="199"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>The dictation will be stopped after
a specified time period</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="198"/>
        <source> minutes</source>
        <translation> minutes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="203"/>
        <source>Character Limit:</source>
        <translation>Character Limit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="213"/>
        <source> characters</source>
        <translation> characters</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <location filename="../widget/startwidget.cpp" line="215"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>The dictation will be stopped after
a specified number of correctly typed
characters</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="219"/>
        <source>Entire
Lesson</source>
        <translation>Entire
Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>The complete lesson will be dictated
from beginning to end</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="222"/>
        <source>(Entire Lesson)</source>
        <translation>(Entire Lesson)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="252"/>
        <source>Response to Typing Errors</source>
        <translation>Response to Typing Errors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="255"/>
        <source>Block Typing Errors</source>
        <translation>Block Typing Errors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="257"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>The dictation will only proceed if the correct
key was pressed</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="260"/>
        <source>Correction with Backspace</source>
        <translation>Correction with Backspace</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="262"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Typing errors have to be removed
with the return key</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="265"/>
        <source>Audible Signal</source>
        <translation>Audible Signal</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="266"/>
        <source>A beep sounds with every typing error</source>
        <translation>A beep sounds with every typing error</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="269"/>
        <source>Show key picture</source>
        <translation>Show key picture</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>For every typing error the corresponding key picture is displayed on the keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="276"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="282"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="289"/>
        <source>Intelligence</source>
        <translation>Intelligence</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="291"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="323"/>
        <source>Assistance</source>
        <translation>Assistance</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="325"/>
        <source>Show Keyboard</source>
        <translation>Show Keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="326"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>For visual support, the virtual keyboard and
status information is shown</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="330"/>
        <source>Colored Keys</source>
        <translation>Colored Keys</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="332"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>For visual support pressing keys will be
marked with colors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="335"/>
        <source>Home Row</source>
        <translation>Home Row</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="337"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>For visual support, the remaining fingers
of the home row will be colored</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>L/R Separation Line</source>
        <translation>L/R Separation Line</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>For visual support a separation line between left
and right hand will be shown</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="347"/>
        <source>Instruction</source>
        <translation>Instruction</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="349"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Show fingers to be used in the status bar</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="352"/>
        <source>Motion Paths</source>
        <translation>Motion Paths</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="354"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Motion paths of the fingers will be shown
on the keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="401"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="402"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Start Training</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="523"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="743"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="773"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="782"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="906"/>
        <source>Please select a text file</source>
        <translation>Please select a text file</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="907"/>
        <source>Text files (*.txt)</source>
        <translation>Text files (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1029"/>
        <source>Please indicate the location of a text file</source>
        <translation>Please indicate the location of a text file</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1078"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="120"/>
        <source>Press space bar to proceed</source>
        <translation>Press space bar to proceed</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="137"/>
        <source>Dictation Finished</source>
        <translation>Dictation Finished</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="99"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="112"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="214"/>
        <source>Press space bar to start</source>
        <translation>Press space bar to start</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="215"/>
        <source>Take Home Row position</source>
        <translation>Take Home Row position</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="231"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Do you really want to exit the lesson early?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="245"/>
        <source>Do you want to save your results?</source>
        <translation>Do you want to save your results?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="462"/>
        <source>E&amp;xit Lesson early</source>
        <translation>E&amp;xit Lesson early</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="515"/>
        <source>Errors: </source>
        <translation>Errors: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="517"/>
        <source>Cpm: </source>
        <translation>Cpm: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="518"/>
        <source>Time: </source>
        <translation>Time: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="520"/>
        <source>Characters: </source>
        <translation>Characters: </translation>
    </message>
</context>
</TS>
