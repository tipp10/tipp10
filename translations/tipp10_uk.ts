<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="68"/>
        <source>Press space bar to start</source>
        <translation>Натисніть пробіл, щоби почати</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="93"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="95"/>
        <source>&amp;Pause</source>
        <translation>&amp;Пауза</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="98"/>
        <source>E&amp;xit Game</source>
        <translation>&amp;Вийти з гри</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="338"/>
        <source>Number of points:</source>
        <translation>Кількість очок:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="403"/>
        <source>Points:</source>
        <translation>Очки:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="402"/>
        <source>Level</source>
        <translation>Рівень</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="414"/>
        <source>Press space bar to proceed</source>
        <translation>Натисніть пробіл, щоби продовжити</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="45"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="73"/>
        <source>Characters</source>
        <translation>Символи</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="74"/>
        <source>Target Errors</source>
        <translation>Цільові помилки</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="75"/>
        <source>Actual Errors</source>
        <translation>Фактичні помилки</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="76"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="77"/>
        <source>Error Rate</source>
        <translation>Кількість помилок</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="80"/>
        <source>This column shows all of the
characters typed</source>
        <translation>У цьому стовпчику показуються всі
введені символи</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="84"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>Символ мав бути надрукований, але не був</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="87"/>
        <source>Character was mistyped</source>
        <translation>Введено невірний символ</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="89"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>У цьому стовпчику вказано загальну частоту кожного
символу, що показується</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>Коефіцієнт помилок показує, які символи викликають
у вас найбільше проблем. Коефіцієнт помилок
обчислюється на основі значення «Цільова помилка»
і значення «Частота».</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="117"/>
        <source>Reset characters</source>
        <translation>Скинути символи</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="191"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Зафіксований коефіцієнт помилок впливає на інтелектуальну функцію та вибір тексту для диктування. Якщо коефіцієнт помилок для певного символу надто високий, може бути корисним скинути список.

Усі записані символи буде видалено.

Ви все ще бажаєте продовжити?
</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="68"/>
        <source>The process will be aborted.</source>
        <translation>Процес буде перервано.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="71"/>
        <source>The update will be aborted.</source>
        <translation>Оновлення буде перервано.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="74"/>
        <source>The program will be aborted.</source>
        <translation>Програма буде перервана.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>Cannot load the program logo.</source>
        <translation>Не вдається завантажити логотип програми.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Не вдається завантажити растрове зображення клавіатури.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="91"/>
        <source>Cannot load the timer background.</source>
        <translation>Не вдається завантажити фон таймера.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="94"/>
        <source>Cannot load the status bar background.</source>
        <translation>Не вдається завантажити фон рядка стану.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Не вдалося знайти базу даних %1. Не вдалося імпортувати файл.
Перевірте, чи це читабельний текстовий файл.</translation>
    </message>
    <message>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation type="vanished">Не вдається знайти базу даних у вказаному каталозі.
TIPP10 намагається створити нову, порожню базу даних у вказаному каталозі.

Ви можете змінити шлях до бази даних у налаштуваннях програми.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Не вдається створити базу даних користувачів у вашому головному каталозі. Можливо, немає дозволу на запис.
TIPP10 намагається використовувати оригінальну базу даних у каталозі програми.

Ви можете змінити шлях до бази даних у налаштуваннях пізніше.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Не вдається створити базу даних користувачів у вказаному каталозі. Можливо, немає каталогу або дозволу на запис.
TIPP10 намагається створити нову, порожню базу даних у вашому головному каталозі.

Ви можете змінити шлях до бази даних у налаштуваннях програми пізніше.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="121"/>
        <location filename="../widget/errormessage.cpp" line="125"/>
        <source>Connection to the database failed.</source>
        <translation>Не вдалося з&apos;єднатися з базою даних.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="128"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>Не вдається очистити таблицю користувача з даними уроків.
Виникла помилка команди SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="132"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>Не вдається очистити таблицю користувача з даними про помилки.
Не вдалося виконати команду SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="136"/>
        <source>No lessons exist.</source>
        <translation>Ніяких уроків немає.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="139"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>Урок не вибрано.
Виберіть урок.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Не вдається створити урок.
Не вдалося виконати команду SQL.</translation>
    </message>
    <message>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Урок не вдалося оновити, оскільки немає
доступу до бази даних.

Якщо ця проблема виникла після того, як програма
працювала безперебійно протягом деякого часу, ймовірно, база даних
була пошкоджена (наприклад, внаслідок збою в роботі комп&apos;ютера).
Щоби перевірити, чи була база даних пошкоджена,
ви можете перейменувати файл бази даних і перезапустити програму (вона
автоматично створить нову, порожню базу даних).
Шлях до бази даних «%1» можна знайти в загальних налаштуваннях.

Якщо ця проблема виникла після першого запуску програми,
перевірте права доступу до
файлу бази даних.</translation>
    </message>
    <message>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Помилки користувача не можуть бути виправлені через
відсутність доступу до бази даних.

Якщо ця проблема виникла після того, як програма
працювала безперебійно протягом деякого часу, ймовірно, база даних
була пошкоджена (наприклад, внаслідок збою в роботі комп&apos;ютера).
Щоб перевірити, чи була база даних пошкоджена,
ви можете перейменувати файл бази даних і перезапустити програму (вона
автоматично створить нову, порожню базу даних).
Шлях до бази даних «%1» можна знайти в загальних налаштуваннях.

Якщо ця проблема виникла після першого запуску програми,
перевірте права доступу до
файлу бази даних.</translation>
    </message>
    <message>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Дані про уроки користувача не можуть бути оновлені, оскільки
немає доступу до бази даних.

Якщо ця проблема виникла після того, як програма
працювала безперебійно протягом деякого часу, ймовірно, база даних
була пошкоджена (наприклад, внаслідок збою в роботі комп&apos;ютера).
Щоб перевірити, чи була база даних пошкоджена,
ви можете перейменувати файл бази даних і перезапустити програму (вона
автоматично створить нову, порожню базу даних).
Шлях до бази даних «%1» можна знайти в загальних налаштуваннях.

Якщо ця проблема виникла після першого запуску програми,
перевірте права доступу до
файлу бази даних.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="104"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="109"/>
        <source>Cannot create the user database in your AppData directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="115"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your AppData directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="146"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="160"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="174"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="187"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Не вдається зберегти урок.
Не вдалося виконати команду SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="190"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Не вдається отримати урок.
Не вдалося виконати команду SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Не вдається проаналізувати урок.
Не вдалося виконати команду SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="196"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>Не вдалося імпортувати файл.
Перевірте, чи це читабельний текстовий файл.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="201"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>Не вдалося імпортувати файл, оскільки він порожній.
Перевірте, чи це читабельний текстовий файл із вмістом.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="206"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>Не вдалося імпортувати файл.
Перевірте правильність написання вебадреси;
це має бути дійсна URL-адреса і читабельний текстовий файл.
Також перевірте з&apos;єднання з мережею.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="211"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>Не вдалося експортувати файл.
Перевірте, чи це доступний для запису текстовий файл.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="215"/>
        <source>Cannot create temporary file.</source>
        <translation>Не вдається створити тимчасовий файл.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="218"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Не вдається виконати процес оновлення.
Перевірте налаштування вашого інтернет-з&apos;єднання та проксі-сервера.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="222"/>
        <source>Cannot read the online update version.</source>
        <translation>Не вдається прочитати онлайн-версію оновлення.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="225"/>
        <source>Cannot read the database update version.</source>
        <translation>Не вдається прочитати версію оновлення бази даних.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="228"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>Не вдається виконати команду SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Не вдається знайти визначення помилок друку.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="234"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Не вдалося створити тимчасовий файл.
Не вдалося оновити.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="237"/>
        <source>Cannot create analysis table.</source>
        <translation>Не вдається створити таблицю аналізу.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="240"/>
        <source>Cannot create analysis index.</source>
        <translation>Не вдається створити індекс аналізу.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="243"/>
        <source>Cannot fill analysis table with values.</source>
        <translation>Не вдається заповнити таблицю аналізу значеннями.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="247"/>
        <source>An error has occured.</source>
        <translation>Виникла помилка.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Код помилки: %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="43"/>
        <source>Report</source>
        <translation>Повідомити</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="47"/>
        <source>Overview of Lessons</source>
        <translation>Огляд уроків</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="48"/>
        <source>Progress of Lessons</source>
        <translation>Поступ уроків</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="49"/>
        <source>Characters</source>
        <translation>Символи</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="50"/>
        <source>Fingers</source>
        <translation>Пальці</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="51"/>
        <source>Comparison Table</source>
        <translation>Порівняльна таблиця</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="72"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрити</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="143"/>
        <source>Use examples</source>
        <translation>Використовувати приклади</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="147"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Зверніть увагу, що ви отримуєте кращі оцінки за повільний набір тексту без помилок, ніж за швидкий набір тексту з великою кількістю помилок!</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="157"/>
        <source>Score</source>
        <translation>Рахунок</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="162"/>
        <source>For example, this equates to …</source>
        <translation>Наприклад, це дорівнює …</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="167"/>
        <source>Performance</source>
        <translation>Продуктивність</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="174"/>
        <location filename="../widget/evaluationwidget.cpp" line="190"/>
        <location filename="../widget/evaluationwidget.cpp" line="201"/>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <location filename="../widget/evaluationwidget.cpp" line="228"/>
        <location filename="../widget/evaluationwidget.cpp" line="239"/>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <location filename="../widget/evaluationwidget.cpp" line="266"/>
        <location filename="../widget/evaluationwidget.cpp" line="277"/>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <location filename="../widget/evaluationwidget.cpp" line="304"/>
        <location filename="../widget/evaluationwidget.cpp" line="315"/>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <location filename="../widget/evaluationwidget.cpp" line="342"/>
        <location filename="../widget/evaluationwidget.cpp" line="353"/>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <location filename="../widget/evaluationwidget.cpp" line="380"/>
        <location filename="../widget/evaluationwidget.cpp" line="391"/>
        <source>Points</source>
        <translation>Бали</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="178"/>
        <location filename="../widget/evaluationwidget.cpp" line="194"/>
        <location filename="../widget/evaluationwidget.cpp" line="205"/>
        <location filename="../widget/evaluationwidget.cpp" line="216"/>
        <location filename="../widget/evaluationwidget.cpp" line="232"/>
        <location filename="../widget/evaluationwidget.cpp" line="243"/>
        <location filename="../widget/evaluationwidget.cpp" line="254"/>
        <location filename="../widget/evaluationwidget.cpp" line="270"/>
        <location filename="../widget/evaluationwidget.cpp" line="281"/>
        <location filename="../widget/evaluationwidget.cpp" line="292"/>
        <location filename="../widget/evaluationwidget.cpp" line="308"/>
        <location filename="../widget/evaluationwidget.cpp" line="319"/>
        <location filename="../widget/evaluationwidget.cpp" line="330"/>
        <location filename="../widget/evaluationwidget.cpp" line="346"/>
        <location filename="../widget/evaluationwidget.cpp" line="357"/>
        <location filename="../widget/evaluationwidget.cpp" line="368"/>
        <location filename="../widget/evaluationwidget.cpp" line="384"/>
        <location filename="../widget/evaluationwidget.cpp" line="395"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 ксх і %2 помилок за %3 хвилини</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="183"/>
        <source>No experience in touch typing</source>
        <translation>Немає досвіду сенсорного набору тексту</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="221"/>
        <source>First steps in touch typing</source>
        <translation>Перші кроки в сенсорному наборі тексту</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="259"/>
        <source>Advanced level</source>
        <translation>Просунутий рівень</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="297"/>
        <source>Suitable skills</source>
        <translation>Відповідні навички</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="335"/>
        <source>Very good skills</source>
        <translation>Дуже хороші навички</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="373"/>
        <source>Perfect skills</source>
        <translation>Досконалі навички</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="233"/>
        <source>Error rates of your fingers</source>
        <translation>Коефіцієнт помилок ваших пальців</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="240"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>Коефіцієнт помилок базується на записаних символах і поточній вибраній розкладці клавіатури.</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>Error Rate:</source>
        <translation>Помилка коефіцієнта:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="253"/>
        <source>Frequency:</source>
        <translation>Частота:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="255"/>
        <source>Errors:</source>
        <translation>Помилок:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="30"/>
        <source>Help</source>
        <translation>Допомога</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="41"/>
        <location filename="../widget/helpbrowser.cpp" line="44"/>
        <source>en</source>
        <translation>ua</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="72"/>
        <source>Back</source>
        <translation>Повернутися</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="75"/>
        <source>Table of Contents</source>
        <translation>Зміст</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="76"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрити</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="79"/>
        <source>&amp;Print page</source>
        <translation>&amp;Сторінка друку</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="118"/>
        <source>Print page</source>
        <translation>Сторінка друку</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="29"/>
        <source>Introduction</source>
        <translation>Введення</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="60"/>
        <source>Welcome to TIPP10</source>
        <translation>Вітаємо в TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="65"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 — це безплатний репетитор сліпого друку для Windows, Mac OS і Linux. Геніальною особливістю програми є її інтелектуальна функція. Символи, які вводяться неправильно, повторюються частіше. Ніколи ще не було так легко навчитися сліпому друку.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="72"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Поради щодо використання 10-пальцевої системи</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. Спочатку поставте пальці у вихідне положення (воно показане на початку кожного уроку). Після натискання кожної клавіші пальці повертаються у вихідне положення.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="82"/>
        <source>en</source>
        <translation>ua</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="86"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Переконайтеся, що ваша постава пряма, і ви не дивитеся на клавіатуру. Ваш погляд завжди повинен бути спрямований на монітор.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="91"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Притисніть плечі до тулуба і опустіть їх донизу. Передпліччя утворюють прямий кут з плечима. Не опускайте зап&apos;ястя вниз і не дозволяйте їм провисати.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="97"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Намагайтеся залишатися розслабленим під час уроків друкування.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="100"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Намагайтеся звести помилки до мінімуму. Швидко друкувати набагато менш ефективно, якщо ви багато помиляєтесь.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="104"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Після того, як ви почнете вводити текст на клавіатурі, вам слід уникати повернення до звичного способу введення (навіть якщо ви поспішаєте).</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="108"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>Якщо вам потрібна допомога у використанні програмного забезпечення, скористайтеся функцією допомоги.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="119"/>
        <source>All rights reserved.</source>
        <translation>Усі права захищені.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="133"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Запускати TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="138"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>&amp;Не показувати мені це вікно знову</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="102"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="103"/>
        <source>&amp;Save</source>
        <translation>&amp;Зберегти</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="112"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Назва уроку (максимум 20 символів):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="114"/>
        <source>Short description (120 characters max.):</source>
        <translation>Короткий опис (максимум 120 символів):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="115"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Зміст уроку (щонайменше два рядки):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="146"/>
        <source>Sentence Lesson</source>
        <translation>Урок з реченнями</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="148"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Кожен рядок завершується
переносом рядка в кінці</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="153"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Кожну одиницю (рядок) буде відокремлено
пропуском. Перенесення рядка відбувається
автоматично після принаймні %1 символу.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="160"/>
        <source>Edit own Lesson</source>
        <translation>Редагувати власний урок</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="226"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Введіть назву уроку
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="232"/>
        <source>Please enter entire lesson content
</source>
        <translation>Введіть весь зміст уроку
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="255"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Введіть максимум 400 рядків тексту
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="263"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>Така назва уроку вже існує. Введіть нову назву.
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="151"/>
        <source>Word Lesson</source>
        <translation>Урок зі словами</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="117"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Пояснення:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;br&gt;Кожен рядок (відокремлений клавішею Enter) еквівалентний одиниці уроку. Існує два типи диктанту уроку:&amp;nbsp;&lt;br&gt;&lt;b&gt;Урок з реченнями&lt;/b&gt; — кожен рядок (речення) буде продиктовано саме так, як він був введений тут, з перенесенням рядка в кінці.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Урок зі словами&lt;/b&gt; — рядки будуть відокремлюватися пробілами, а перенесення рядка буде автоматичне принаймні після %1 символів.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="127"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;Що відбувається, коли ввімкнено «інтелектуальну» функцію?&lt;/b&gt;&lt;br&gt;З увімкненою інтелектуальною функцією рядки для диктування вибиратимуться залежно від квоти помилок замість того, щоб диктувати їх у правильному порядку. Увімкнення «інтелекту» має сенс лише в тому випадку, якщо урок складається з багатьох рядків (часто це «Уроки зі словом»).&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="135"/>
        <source>Dictate the text as:</source>
        <translation>Диктувати текст як:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="162"/>
        <source>Name of the Lesson:</source>
        <translation>Назва уроку:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="166"/>
        <source>Create own Lesson</source>
        <translation>Створити власний урок</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="249"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Введіть принаймні два рядки тексту
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="25"/>
        <source>Print Lesson</source>
        <translation>Урок друкування</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="45"/>
        <source>&amp;Print</source>
        <translation>&amp;Друкувати</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="46"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="60"/>
        <source>Please enter your name:</source>
        <translation>Введіть своє ім&apos;я:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="51"/>
        <source>Print</source>
        <translation>Друкувати</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="83"/>
        <source>Entire Lesson</source>
        <translation>Увесь урок</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="178"/>
        <source>%L1 min</source>
        <translation>%L1 хв</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="254"/>
        <location filename="../widget/lessonresult.cpp" line="449"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>Ви досягли %1 зі швидкістю набору %2 ксх і зробили %n помилку.</numerusform>
            <numerusform>Ви досягли %1 зі швидкістю набору %2 ксх і зробили %n помилки.</numerusform>
            <numerusform>Ви досягли %1 зі швидкістю набору %2 ксх і зробили %n помилок.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="272"/>
        <location filename="../widget/lessonresult.cpp" line="467"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="283"/>
        <location filename="../widget/lessonresult.cpp" line="330"/>
        <location filename="../widget/lessonresult.cpp" line="479"/>
        <location filename="../widget/lessonresult.cpp" line="525"/>
        <source>Duration: </source>
        <translation>Тривалість: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="287"/>
        <location filename="../widget/lessonresult.cpp" line="483"/>
        <source>Typing Errors: </source>
        <translation>Помилки друку: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="291"/>
        <location filename="../widget/lessonresult.cpp" line="487"/>
        <source>Assistance: </source>
        <translation>Допомога: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="305"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Results</source>
        <translation>Результати</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="322"/>
        <location filename="../widget/lessonresult.cpp" line="517"/>
        <source>Lesson: </source>
        <translation>Урок: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="326"/>
        <location filename="../widget/lessonresult.cpp" line="521"/>
        <source>Time: </source>
        <translation>Час: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="334"/>
        <location filename="../widget/lessonresult.cpp" line="529"/>
        <source>Characters: </source>
        <translation>Символи: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="338"/>
        <location filename="../widget/lessonresult.cpp" line="533"/>
        <source>Errors: </source>
        <translation>Помилок: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="342"/>
        <location filename="../widget/lessonresult.cpp" line="537"/>
        <source>Error Rate: </source>
        <translation>Коефіцієнт помилок: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="346"/>
        <location filename="../widget/lessonresult.cpp" line="541"/>
        <source>Cpm: </source>
        <translation>Символів/хв: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="360"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Dictation</source>
        <translation>Диктант</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="443"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>TIPP10 Посібник зі сліпого друку</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source>Report</source>
        <translation>Надіслати</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source> of %1</source>
        <translation> з %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="571"/>
        <source>Print Report</source>
        <translation>Роздрукувати звіт</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="73"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n хвилина</numerusform>
            <numerusform>%n хвилини</numerusform>
            <numerusform>%n хвилин</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="78"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n символ</numerusform>
            <numerusform>%n символи</numerusform>
            <numerusform>%n символів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="90"/>
        <source>Error Correction with Backspace</source>
        <translation>Виправлення помилок за допомогою Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="93"/>
        <source>Error Correction without Backspace</source>
        <translation>Виправлення помилок без Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="95"/>
        <source>Ignore Errors</source>
        <translation>Ігнорувати помилки</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>None</source>
        <translation>Немає</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="111"/>
        <source>All</source>
        <translation>Усе</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="119"/>
        <source>- Colored Keys</source>
        <translation>- Кольорові клавіші</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="125"/>
        <source>- Home Row</source>
        <translation>- Головний ряд</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="131"/>
        <source>- Motion Paths</source>
        <translation>- Лінія руху пальців</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="137"/>
        <source>- Separation Line</source>
        <translation>- Лінія поділу</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="143"/>
        <source>- Instructions</source>
        <translation>- Інструкції</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="175"/>
        <source>%L1 s</source>
        <translation>%L1 с</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n бал</numerusform>
            <numerusform>%n бали</numerusform>
            <numerusform>%n балів</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="69"/>
        <source>%L1 s</source>
        <translation>%L1 с</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="73"/>
        <source>%L1 min</source>
        <translation>%L1 хв</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="77"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="89"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n бал</numerusform>
            <numerusform>%n бали</numerusform>
            <numerusform>%n балів</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="126"/>
        <source>Show: </source>
        <translation>Поглянути: </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="128"/>
        <source>All Lessons</source>
        <translation>Усі уроки</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="129"/>
        <source>Training Lessons</source>
        <translation>Навчальні уроки</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="130"/>
        <source>Open Lessons</source>
        <translation>Відкриті уроки</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="131"/>
        <source>Own Lessons</source>
        <translation>Власні уроки</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="266"/>
        <source>Lesson</source>
        <translation>Урок</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="267"/>
        <source>Time</source>
        <translation>Час</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="268"/>
        <source>Duration</source>
        <translation>Тривалість</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="269"/>
        <source>Characters</source>
        <translation>Символи</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="270"/>
        <source>Errors</source>
        <translation>Помилки</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="271"/>
        <source>Rate</source>
        <translation>Коефіцієнт</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="272"/>
        <source>Cpm</source>
        <translation>Ксх</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="273"/>
        <source>Score</source>
        <translation>Рахунок</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="276"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>У цьому стовпчику показано назви
завершених уроків</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="280"/>
        <source>Start time of the lesson</source>
        <translation>Час початку уроку</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="282"/>
        <source>Total duration of the lesson</source>
        <translation>Загальна тривалість уроку</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="283"/>
        <source>Number of characters dictated</source>
        <translation>Кількість продиктованих символів</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="286"/>
        <source>Number of typing errors</source>
        <translation>Кількість помилок друку</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>Коефіцієнт помилок розраховується наступним чином:
Помилки / Символи
Чим менший коефіцієнт помилок, тим краще!</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>«КСХ» показує кількість символів за хвилину, які
було введено в середньому</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>Рахунок вираховується наступним чином:
((Символи - (20 x Помилки)) / Тривалість у хвилинах) x 0.4

Зверніть увагу, що повільний набір тексту без помилок дає
кращий результат, ніж швидкий набір тексту з кількома помилками!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="61"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>Всі результати поточного уроку будуть анульовані!

Справді бажаєте вийти?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="82"/>
        <location filename="../widget/mainwindow.cpp" line="100"/>
        <source>&amp;Go</source>
        <translation>&amp;Перейти</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="88"/>
        <location filename="../widget/mainwindow.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="96"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Settings</source>
        <translation>&amp;Налаштування</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="116"/>
        <source>E&amp;xit</source>
        <translation>&amp;Вийти</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="118"/>
        <source>&amp;Results</source>
        <translation>&amp;Результати</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Manual</source>
        <translation>&amp;Посібник</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="121"/>
        <source> on the web</source>
        <translation> в мережі</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="123"/>
        <source>Info</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="125"/>
        <source>&amp;About </source>
        <translation>&amp;Про нас </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="128"/>
        <source>ABC-Game</source>
        <translation>Гра ABC</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="182"/>
        <source>Software Version </source>
        <translation>Версія ПЗ </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="183"/>
        <source>Database Version </source>
        <translation>Версія бази даних </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="188"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Інтелектуальний репетитор сліпого друку</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="189"/>
        <source>On the web: </source>
        <translation>У мережі: </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="191"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 публікується під ліцензією GNU General Public License і доступний безплатно. Вам не потрібно платити за нього, де б ви його не завантажили!</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="201"/>
        <source>About </source>
        <translation>Про нас </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="185"/>
        <source>Portable Version</source>
        <translation>Портативна версія</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="39"/>
        <location filename="../widget/progressionwidget.cpp" line="50"/>
        <location filename="../widget/progressionwidget.cpp" line="504"/>
        <source>Time</source>
        <translation>Час</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="42"/>
        <source>Show: </source>
        <translation>Показати: </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="44"/>
        <source>All Lessons</source>
        <translation>Усі уроки</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="45"/>
        <source>Training Lessons</source>
        <translation>Навчальні уроки</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="46"/>
        <source>Open Lessons</source>
        <translation>Відкриті уроки</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="47"/>
        <source>Own Lessons</source>
        <translation>Власні уроки</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <source>Order by x-axis:</source>
        <translation>Упорядкувати по осі Х:</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="51"/>
        <location filename="../widget/progressionwidget.cpp" line="510"/>
        <source>Lesson</source>
        <translation>Урок</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="229"/>
        <source>Points</source>
        <translation>Бали</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="360"/>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <source>Training Lesson</source>
        <translation>Навчальний урок</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="367"/>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <source>Open Lesson</source>
        <translation>Відкритий урок</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="374"/>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <source>Own Lesson</source>
        <translation>Власний урок</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="455"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n бал</numerusform>
            <numerusform>%n бали</numerusform>
            <numerusform>%n балів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="456"/>
        <source>%1 cpm</source>
        <translation>%1 ксх</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="473"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>Графік поступу буде показано після завершення двох уроків.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="515"/>
        <source>Cpm</source>
        <translation>Ксх</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="520"/>
        <source>Score</source>
        <translation>Рахунок</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="97"/>
        <location filename="../sql/connection.h" line="105"/>
        <location filename="../sql/connection.h" line="125"/>
        <source>Affected directory:
</source>
        <translation>Постраждалий каталог:
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>en</source>
        <translation>ua</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="31"/>
        <location filename="../widget/fingerwidget.cpp" line="120"/>
        <source>Left little finger</source>
        <translation>Лівий мізинець</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="32"/>
        <location filename="../widget/fingerwidget.cpp" line="121"/>
        <source>Left ring finger</source>
        <translation>Лівий безіменний</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="33"/>
        <location filename="../widget/fingerwidget.cpp" line="122"/>
        <source>Left middle finger</source>
        <translation>Лівий середній</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="123"/>
        <source>Left forefinger</source>
        <translation>Лівий вказівний</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="124"/>
        <source>Right forefinger</source>
        <translation>Правий вказівний</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="35"/>
        <location filename="../widget/fingerwidget.cpp" line="125"/>
        <source>Right middle finger</source>
        <translation>Правий середній</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="36"/>
        <location filename="../widget/fingerwidget.cpp" line="126"/>
        <source>Right ring finger</source>
        <translation>Правий безіменний</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Right little finger</source>
        <translation>Правий мізинець</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Thumb</source>
        <translation>Великий палець</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation>Фільтр для розкладки клавіатури</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation>Обмеження на кількість символів</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>Намагайтеся уникати використання символів, які не підтримуються вашою розкладкою клавіатури. Фільтр у вигляді регулярного виразу застосовується до всіх текстів вправ перед початком уроку. Ви можете вносити зміни тільки якщо ви знайомі з регулярними виразами.</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation>Змінний фільтр</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>Фільтрування недозволених символів може призвести до створення текстів, які не мають сенсу (наприклад, вилучення умлаутів). Ви можете визначити заміни, які буде застосовано до того, як буде застосовано обмеження символів. Скористайтеся наведеним тут прикладом, який замінює всі німецькі умлаути і символ ß:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="74"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Деякі з ваших налаштувань потребують перезапуску програми, щоби набути чинності.
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="291"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>Всі дані завершених уроків для поточного користувача будуть видалені,
а список уроків повернеться до початкового стану після первинного встановлення!

Ви дійсно хочете продовжити?

</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="305"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>Дані завершених уроків успішно видалено!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="316"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>Усі записані символи (квоти помилок) поточного користувача будуть видалені, а список символів повернеться до початкового стану після первинноїґго встановлення!

Ви дійсно бажаєте продовжити?</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="330"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>Записані символи успішно видалено!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Навчання</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation>Тикер</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation>Тут ви можете вибрати колір фону</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation>Тут ви можете вибрати колір шрифту</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation>Курсор:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Тут ви можете змінити шрифт тикера (розмір шрифту більше 20 pt не рекомендується)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>&amp;Змінити шрифт</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Тут можна вибрати колір курсора для поточного символу</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Шрифт:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation>Колір шрифту:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation>Фон:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Швидкість:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Вимк.</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Тут ви можете змінити швидкість тикера (повзунок зліва): тикер не рухається, поки не досягне кінця рядка. Повзунок праворуч: тикер рухається дуже швидко.)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Швидко</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Аудіовихід</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation>Тут можна активувати метроном</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation>Метроном:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation>Виберіть, як часто повинен лунати звук метронома на хвилину</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation> ксх</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation>Розширені</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Навчальні уроки:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>Вибрані вами навчальні уроки не підходять для розкладки клавіатури. Ви можете продовжити, але, можливо, вам доведеться ігнорувати деякі клавіші із самого початку.</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation>Розкладка клавіатури:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation>Результати</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>Дані користувача</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Тут ви можете скинути всі збережені дані уроків (уроки будуть порожніми, як вони були після першого встановлення)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation>&amp;Скинути завершені уроки</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Тут ви можете скинути всі записані натискання клавіш і помилки друку (символи будуть порожніми, як вони були після початкового встановлення)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>&amp;Скинути всі записані символи</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation>Інші</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation>Вікна</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Тут ви можете вирішити, чи показувати інформаційне вікно з підказками на початку</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation>Показувати вітальне повідомлення під час запуску</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Тут ви можете визначити, чи показуватиметься вікно попередження, коли запускається відкритий або власний урок з активованим інтелектом</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Пам&apos;ятайте про ввімкнений інтелект перед початком
відкритого або власного уроку</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Автоматично змінювати тривалість уроку на
«Цілий урок» при вимкненні інтелекту</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="87"/>
        <location filename="../widget/startwidget.cpp" line="731"/>
        <source>Training Lessons</source>
        <translation>Навчальні уроки</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="94"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="146"/>
        <source>&amp;Edit</source>
        <translation>&amp;Редагувати</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="150"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;Новий урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="152"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Імпортувати урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="154"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Експортувати урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="156"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Редагувати урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="158"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Видалити урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="187"/>
        <source>Duration of Lesson</source>
        <translation>Тривалість уроку</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="190"/>
        <source>Time Limit:</source>
        <translation>Обмеження часу:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="192"/>
        <location filename="../widget/startwidget.cpp" line="199"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>Диктант буде зупинено через
вказаний проміжок часу</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="198"/>
        <source> minutes</source>
        <translation> хвилин</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="203"/>
        <source>Character Limit:</source>
        <translation>Обмеження символів:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="213"/>
        <source> characters</source>
        <translation> символів</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <location filename="../widget/startwidget.cpp" line="215"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>Диктант буде зупинено після
вказаної кількості правильно набраних
символів</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="219"/>
        <source>Entire
Lesson</source>
        <translation>Цілий
урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>Весь урок буде продиктований
від початку до кінця</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="222"/>
        <source>(Entire Lesson)</source>
        <translation>(Цілий урок)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="252"/>
        <source>Response to Typing Errors</source>
        <translation>Реакція на помилки друку</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="255"/>
        <source>Block Typing Errors</source>
        <translation>Блокувати друкарські помилки</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="257"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>Диктант буде продовжено, тільки якщо
було натиснуто правильну клавішу</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="260"/>
        <source>Correction with Backspace</source>
        <translation>Виправлення за допомогою Backspace</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="262"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Помилки друку будуть видалятися
за допомогою клавіші повернення</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="265"/>
        <source>Audible Signal</source>
        <translation>Звуковий сигнал</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="266"/>
        <source>A beep sounds with every typing error</source>
        <translation>Звуковий сигнал лунає при кожній помилці введення</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="269"/>
        <source>Show key picture</source>
        <translation>Показати зображення клавіші</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>Для кожної помилки на клавіатурі показуватиметься відповідне зображення клавіші</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="276"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Текст уроку не буде надиктований у передбачуваній послідовності, а буде коригуватися в реальному часі з урахуванням ваших помилок при друкуванні.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="282"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Виберіть цей параметр, якщо текст уроку не буде надиктований у передбачуваній послідовності, а буде коригуватися в реальному часі з урахуванням ваших помилок набору.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="289"/>
        <source>Intelligence</source>
        <translation>Інтелект</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="291"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>На основі поточного коефіцієнту помилок всі символи, слова і фрази для диктанту будуть підбиратися в режимі реального часу. З іншого боку, якщо прапорець не встановлений, текст уроку завжди буде диктуватися в тому ж порядку, що і раніше.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="323"/>
        <source>Assistance</source>
        <translation>Допомога</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="325"/>
        <source>Show Keyboard</source>
        <translation>Показати клавіатуру</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="326"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>Для візуальної підтримки показано віртуальна клавіатура та
інформація про стан</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="330"/>
        <source>Colored Keys</source>
        <translation>Кольорові клавіші</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="332"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>Для візуальної допомоги натискання клавіш буде
позначено кольорами</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="335"/>
        <source>Home Row</source>
        <translation>Головний ряд</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="337"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>Для візуальної допомоги решта пальців
головного ряду будуть кольоровими</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>L/R Separation Line</source>
        <translation>Роздільна лінія ліворуч/праворуч</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>Для візуальної допомоги
буде показано лінію поділу між лівою та правою рукою</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="347"/>
        <source>Instruction</source>
        <translation>Інструкція</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="349"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Показати пальці для використання в рядку стану</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="352"/>
        <source>Motion Paths</source>
        <translation>Лінія руху пальців</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="354"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Лінію руху пальців буде показано
на клавіатурі</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="401"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="402"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Почати урок</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="523"/>
        <source>All</source>
        <translation>Усе</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="743"/>
        <source>Open Lessons</source>
        <translation>Відкриті уроки</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="773"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>Наразі відкриті уроки існують лише німецькою мовою. Ми сподіваємось, що незабаром відкриті уроки з&apos;являться і англійською.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="782"/>
        <source>Own Lessons</source>
        <translation>Власні уроки</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="906"/>
        <source>Please select a text file</source>
        <translation>Виберіть текстовий файл</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="907"/>
        <source>Text files (*.txt)</source>
        <translation>Текстові файли (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1029"/>
        <source>Please indicate the location of a text file</source>
        <translation>Укажіть місце розташування текстового файлу</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1078"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Дійсно хочете видалити урок і всі записані дані в контексті цього уроку?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="120"/>
        <source>Press space bar to proceed</source>
        <translation>Натисніть пробіл, щоби продовжити</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="137"/>
        <source>Dictation Finished</source>
        <translation>Диктант закінчено</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="99"/>
        <source>&amp;Pause</source>
        <translation>&amp;Пауза</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="112"/>
        <source>&amp;Help</source>
        <translation>&amp;Допомога</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="214"/>
        <source>Press space bar to start</source>
        <translation>Натисніть пробіл, щоби почати</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="215"/>
        <source>Take Home Row position</source>
        <translation>Займіть положення на головному ряду</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="231"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Бажаєте покинути урок раніше?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="245"/>
        <source>Do you want to save your results?</source>
        <translation>Бажаєте зберегти результати?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="462"/>
        <source>E&amp;xit Lesson early</source>
        <translation>&amp;Покинути урок раніше</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="515"/>
        <source>Errors: </source>
        <translation>Помилок: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="517"/>
        <source>Cpm: </source>
        <translation>Ксх: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="518"/>
        <source>Time: </source>
        <translation>Час: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="520"/>
        <source>Characters: </source>
        <translation>Символів: </translation>
    </message>
</context>
</TS>
