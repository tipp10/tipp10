/*
Copyright (c) 2006-2011, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** File name: main.cpp
**
****************************************************************/

#include <QApplication>
#include <QCoreApplication>
#include <QLibraryInfo>
#include <QSettings>
#include <QString>
#include <QTranslator>

#include "def/defines.h"
#include "sql/connection.h"
#include "widget/illustrationdialog.h"
#include "widget/mainwindow.h"

int main(int argc, char* argv[])
{

    QApplication app(argc, argv);

    // Set application name and domain
    // (this saves having to repeat the information
    // each time a QSettings object is created)
    QApplication::setOrganizationName(t10::app_name_intern);
    QApplication::setOrganizationDomain(t10::app_url);
    QApplication::setApplicationName(t10::app_name_intern);
    QApplication::setDesktopFileName("com.gitlab.tipp10.tipp10");
    QApplication::setWindowIcon(QIcon(":/img/icon.svg"));

    // Translation
    // Common qt widgets
    QTranslator translatorQt;
    bool loaded = translatorQt.load("qt_" + QLocale::system().name(),
        QLibraryInfo::path(QLibraryInfo::TranslationsPath));
    if (loaded)
        QApplication::installTranslator(&translatorQt);

    // Content (own translation files)
    QString trFile = "tipp10_" + QLocale::system().name();
    QString trPath = t10::getDataDir() + "translations";
    QTranslator translatorContent;
    if (translatorContent.load(trFile, trPath))
        QApplication::installTranslator(&translatorContent);

// Settings to get and set general settings
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;

    // move settings to new location
    QSettings oldSettings("Tom Thielicke IT Solutions", t10::app_name_intern);
    QFile oldconfig(oldSettings.fileName());
    if (oldconfig.exists()) {
        QDir oldDir = QFileInfo(oldconfig).dir();

        QFile newconfig(settings.fileName());
        QDir dir = QFileInfo(newconfig).dir();
        dir.mkpath(dir.path());

        oldconfig.rename(settings.fileName());
        oldDir.rmdir(oldDir.path());
        settings.sync();
    }
#endif

    // Read/write language, license key and show illustration flag
    settings.beginGroup("main");
    QString languageLayout = settings.value("language_layout", "").toString();

    QString languageLesson = settings.value("language_lesson", "").toString();

    bool showIllustration = settings.value("check_illustration", true).toBool();
    settings.endGroup();

    if (languageLayout == "") {
        if (QObject::tr("en") == "de") {
            languageLesson = "de_de_qwertz";
            languageLayout = "de_qwertz";
        } else {
            languageLesson = "en_us_qwerty";
            languageLayout = "us_qwerty";
        }
#ifdef APP_MAC
        languageLayout.append("_mac");
#else
        languageLayout.append("_win");
#endif
    }

    settings.beginGroup("main");
    settings.setValue("language_layout", languageLayout);
    settings.setValue("language_lesson", languageLesson);
    settings.endGroup();

    // Create database connection
    if (!createConnection()) {
        // Cannot find or open database
        // -> exit program
        return 1;
    }

    // Show illustration widget at the beginning if not disabled by the user
    if (showIllustration) {
        IllustrationDialog illustrationDialog(nullptr);
        illustrationDialog.exec();
    }

    // Create main window object
    MainWindow window;
    window.show();

    // Start the event loop
    return QApplication::exec();
}
